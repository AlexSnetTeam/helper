# ~*~ coding: utf-8 ~*~
import os, sys
import subprocess

PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))

PYINSTALLER_PATH = os.path.join(PROJECT_ROOT,'compiler/pyinstaller')
UPX_PATH = os.path.join(PROJECT_ROOT,'compiler/upx_%s'%os.name)

sys.path.append(PYINSTALLER_PATH)


def extra_datas(mydir):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % mydir, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'DATA'))

    return extra_datas

# spec_cmd = "%(pyexe)s %(pyinstaller)s/utils/Makespec.py --onefile --debug --name=%(appname)s --paths=%(proot)s/src/plugins --paths=%(proot)s/src/libraries src/main.py"%{
#     'pyexe' : sys.executable,
#     'pyinstaller' : PYINSTALLER_PATH,
#     'proot' : PROJECT_ROOT,
#     'upx' : UPX_PATH,
#     'appname' : 'Helper',

# }
# build_cmd = "%(pyexe)s %(pyinstaller)s/utils/Build.py --upx-dir=%(upx)s --noconfirm helper.spec"%{
#     'pyexe' : sys.executable,
#     'pyinstaller' : PYINSTALLER_PATH,
#     'proot' : PROJECT_ROOT,
#     'upx' : UPX_PATH,
#     'appname' : 'Helper',

# }
# subprocess.call(spec_cmd, shell=True)
# subprocess.call(build_cmd, shell=True)

