# ~*~ coding: utf-8 ~*~
import os
import sys
import logging


class Application(object):

    DEBUG = True
    QT_API = None

    qtapp = None
    mainWindow = None
    trayIcon = None

    pluginManager = None

    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Application, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        if os.environ.has_key('PYCHARM_HOSTED'):
            self.DEBUG = True

        if not self.qtapp:
            self.setupLogger()

            self.setupQtEnviroment()
            self.setupImportHook()
            self.setupPluginManager()

            self.runGui()

            # If main
            self.mainGuiLoop()
            # self.createTrayIcon()

            self.pluginManager.loadPlugins()
            self.executeGui(self.qtapp.exec_())

    @property
    def version(self):
        return '0.12.27'

    @property
    def project_directory(self):
        if getattr(sys, 'frozen', None):
             basedir = sys._MEIPASS
        else:
             basedir = os.path.dirname(os.path.abspath(__file__))
        return os.path.dirname(basedir)

    def replaceIncludePath(self):
        sys.path.insert(0, self.project_directory )
        PLUG_ROOT = os.path.join(self.project_directory, 'plugins')
        LIBS_ROOT = os.path.join(self.project_directory, 'libraries')
        sys.path.insert(0, PLUG_ROOT)
        sys.path.insert(0, LIBS_ROOT)

    def chooseGui(self):
        pass

    def showMainWindow(self):
        if not self.mainWindow:
            self.pluginManager.loadPlugin('main')
            return True
        raise ImportError

    def mainGuiLoop(self):
        self.showMainWindow()

    def runGui(self):
        self.logger.info('Running GUI')
        from QTF import QtGui, QtCore

        self.qtapp = QtGui.QApplication(sys.argv)
        self.qtapp.setWindowIcon(QtGui.QIcon('resources/mailru.png'))

        self.qtapp.setApplicationName(u'Helper')
        self.qtapp.setOrganizationDomain(u'codeteam.ru')
        self.qtapp.setOrganizationName(u'CodeTeam')
        self.qtapp.setApplicationVersion(self.version)

    def executeGui(self, r):
        self.logger.info('Unloading all loaded plugins')
        if self.pluginManager:
            plk = self.pluginManager._plugins.keys()
            for p in plk:
                self.pluginManager.unloadPlugin(p)
        self.logger.info('Exiting GUI application with status code %i'%r)
        sys.exit(r)

    def setupLogger(self):
        import StringIO

        self._stdout = sys.stdout
        self._stderr = sys.stderr

        self.stdout = StringIO.StringIO()
        self.stderr = StringIO.StringIO()

        sys.stdout = self.stdout
        sys.stderr = self.stderr

        self.logger = logging.getLogger('')
        self.logger.setLevel(logging.INFO)

        if self.DEBUG:
            # Adding console output if running in Debug mode
            ch = logging.StreamHandler(self._stdout)
            ch.setLevel(logging.DEBUG)
            formatter = logging.Formatter('[%(levelname)-8s] %(asctime)s [%(name)s] %(message)s')
            ch.setFormatter(formatter)
            self.loggerHandler = ch
            self.logger.addHandler(self.loggerHandler)
            self.logger.debug('Running in debug mode')


    def setupPluginManager(self):
        self.logger.info('Loading PluginManager')
        from plugins import PluginManager
        self.pluginManager = PluginManager(self)


    def createTrayIcon(self):
        from QTF import QtGui, QtCore

        if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
            return False

        # self.qtapp.setQuitOnLastWindowClosed(False)
        if not hasattr(self, 'trayIconMenu'):
            self.trayIconMenu = QtGui.QMenu(self.mainWindow)
        self.trayIconMenu.addAction(self.mainWindow.restoreAction)
        self.trayIconMenu.addSeparator()
        #self.trayIconMenu.addAction(self.mainWindow.restoreAction)
        #self.trayIconMenu.addAction(self.mainWindow.restoreAction)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.mainWindow.quitAction)

        self.trayIcon = QtGui.QSystemTrayIcon(self.mainWindow)
        self.trayIcon.setContextMenu(self.trayIconMenu)
        self.trayIcon.setIcon(QtGui.QIcon('./resources/atSign.png'))
        self.trayIcon.show()
        #self.trayIcon.clicked.connect(self.show)
        #self.trayIcon.showMessage(u'Test',u'Message')

    def showMessage(self,title,message):
        if self.trayIcon:
            self.trayIcon.showMessage(title,message)

    def setupImportHook(self):
        """
        Magick!
        Переопределяю функцию импорта.
        Теперь если импортировать из пакета MRD.{название плагина}
          то автоматически все подставиться и будет импортирован плагин
        
        Пример:
        from MRD.confluence import window
        >>> from plugins.confluencePlugin import window
        
        from QTF import QtGui, QtCore
        >>> from PySide import QtGui, QtCore
        >>> from QtGui4 import QtGui, QtCore
        
        При этом должна автоматически пройти проверка на наличие подобного плагина
          и разрешение на загрузку. То есть, если пользователь запретит загрузку,
          то срайзится ImportError не смотря на реальное наличие данного пакета.
        
        """
        self.logger.info('Setuping import hoook')

        import os, sys
        sys.path.insert(0, os.path.join(os.path.dirname(sys.argv[0]), 'plugins'))
        sys.path.insert(0, os.path.join(os.path.dirname(sys.argv[0]), 'libraries'))
        self.logger.info('Working around system path',extra={'path':sys.path})

        import __builtin__
        __import__ = __builtin__.__import__
        def hook(name, globals=None, locals=None, fromlist=None, level=-1):
            root, sep, other = name.partition('.')
            if root == 'MRD':
                if self.pluginManager.loadPlugin(other):
                    name = 'plugins' + sep + other + 'Plugin'
                else:
                    raise ImportError
            elif root == 'QTF':
                name = self.QT_API + sep + other

            return __import__(name, globals, locals, fromlist, level)
        __builtin__.__import__ = hook

    def setupQtEnviroment(self):
        QT_API_PYQT = 'PyQt4'
        QT_API_PYSIDE = 'PySide'

        self.logger.info('Trying to find Qt python bindings')

        def prepare_pyqt4():
            """
            For PySide compatibility, use the new-style string API that automatically
            converts QStrings to Unicode Python strings. Also, automatically unpack
            QVariants to their underlying objects.
            """
            import sip
            sip.setapi('QString', 2)
            sip.setapi('QVariant', 2)

        # Select Qt binding, using the QT_API environment variable if available.
        QT_API = os.environ.get('QT_API')
        if QT_API is None:
            try:
                import PySide
                if PySide.__version__ < '1.0.3':
                    # old PySide, fallback on PyQt
                    raise ImportError
                QT_API = QT_API_PYSIDE
            except ImportError:
                try:
                    prepare_pyqt4()
                    import PyQt4
                    from PyQt4 import QtCore
                    if QtCore.PYQT_VERSION_STR < '4.7':
                        # PyQt 4.6 has issues with null strings returning as None
                        raise ImportError
                    QT_API = QT_API_PYQT
                except ImportError:
                    raise ImportError('Cannot import PySide >= 1.0.3 or PyQt4 >= 4.7')

        elif QT_API == QT_API_PYQT:
            prepare_pyqt4()

        if QT_API == QT_API_PYQT:
            from PyQt4 import QtCore, QtGui, QtSvg
            if QtCore.PYQT_VERSION_STR < '4.7':
                raise ImportError("Helper requires PyQt4 >= 4.7, found %s"%QtCore.PYQT_VERSION_STR)
            QtCore.Signal = QtCore.pyqtSignal
            QtCore.Slot = QtCore.pyqtSlot
            import PyQt4
            PyQt4._ = QtCore.QObject().tr
        elif QT_API == QT_API_PYSIDE:
            import PySide
            if PySide.__version__ < '1.0.3':
                raise ImportError("Helper requires PySide >= 1.0.3, found %s"%PySide.__version__)
            from PySide import QtCore, QtGui
            PySide._ = QtCore.QObject().tr
        else:
            raise RuntimeError('Invalid Qt API %r, valid values are: %r or %r' % (QT_API, QT_API_PYQT, QT_API_PYSIDE))

        self.logger.info('Using %s as default Qt wrapper'%QT_API)
        self.QT_API = QT_API