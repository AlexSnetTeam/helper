#-*- coding:utf-8 -*-
import logging
import re
from plugins import PluginInterface

logger = logging.getLogger('Helper.Plugins.newslinePlugin')

from QTF import QtGui

class Newsline(PluginInterface):
    def __init__(self, plugin_manager):
        super(Newsline,self).__init__(plugin_manager)

        logger.info('initializing')

        self.app = self.pm.getApplication()
        self.mw = self.pm.getMainWindow()

        from .feedUpdater import FeedAggregator
        self.feedthread = FeedAggregator()
        self.feedthread.newFeed.connect(self.updateFeed)
        self.feedthread.start()


    def unload(self):
        logger.info('unloading')

        self.mw.update()
        self.feedthread.terminate()
        self.feedthread.deleteLater()

    def updateFeed(self):
        logger.info('feed updated')
        self.newsline = self.feedthread.currentStateFeed
        # for n in self.newsline[:4]:
        #     self.app.showMessage(n['title'],n['preview'])
