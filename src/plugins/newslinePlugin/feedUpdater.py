#-*- coding:utf-8 -*-
import logging
import time
from QTF import QtCore, QtGui

logger = logging.getLogger('Helper.Plugins.newslinePlugin.feedUpdater')

class FeedAggregator(QtCore.QThread):
    newFeed = QtCore.Signal(list())
    currentStateFeed = list()

    def __init__(self, parent=None):
        super(FeedAggregator,self).__init__(parent)
        from . import feedparser
        self.feedparser = feedparser
        logger.info('thread started')

    def run(self):
        while True:
            logger.info('thread loops')
            feed = list()
            try:
                fdp = self.feedparser.parse('http://news.mail.ru/rss/main/')
                for e in fdp.entries:
                    f = {
                        'title' : e.title,
                        'preview': e.summary,
                        'link'  : e.link,
                        'date'  : e.published_parsed
                    }
                    feed.append(f)
                self.currentStateFeed=feed
                self.newFeed.emit()
            except Exception, e:
                logger.warning('catch error: %s'%e)
            time.sleep(30)

