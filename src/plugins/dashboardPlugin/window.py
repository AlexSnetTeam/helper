#-*- coding:utf-8 -*-

from QTF import QtGui, QtCore, QtUiTools

class DashboardWindow(QtGui.QWidget):
    def __init__(self, plugin):
        super(DashboardWindow, self).__init__()
        self.setupLayout()
        self.plugin = plugin
        self.loader = QtUiTools.QUiLoader()
        self.ui = self.loader.load(self.plugin.resourcePath + "/form.ui")
        self.addWidget(self.ui)
        self.ui.show()
        # self.w.move(100,100)

    def setupLayout(self):
    	self.wm = QtGui.QVBoxLayout()
    	self.setLayout(self.wm)

    def addWidget(self, widget):
    	self.wm.addWidget(widget)