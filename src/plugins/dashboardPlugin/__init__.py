#-*- coding:utf-8 -*-

from QTF import QtCore
from QTF import QtGui
from QTF import _

from plugins import PluginInterface


class Dashboard(PluginInterface):
    def __init__(self, plugin_manager):
        super(Dashboard, self).__init__(plugin_manager)

        from .window import DashboardWindow
        self.dashboard = DashboardWindow(self)
        self.pm.getMainWindow().wm.addWidget(self.dashboard)

        self.dashboardAct = QtGui.QAction(QtGui.QIcon("%s/dashboard.png"%self.resourcePath), _("Dash"), self.pm.getMainWindow(),triggered=self.showDashboard)
        self.pm.mainPlugin.addSidebarButton(self.dashboardAct)

        self.showDashboard()

    def unload(self):
        pass

    def showDashboard(self):
        self.pm.getMainWindow().wm.setCurrentWidget(self.dashboard)
