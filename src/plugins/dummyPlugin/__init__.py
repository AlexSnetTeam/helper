#-*- coding:utf-8 -*-
from plugins import PluginInterface

# Main plugin class must be equal to plugin title, which declared in Info.xml
class Dummy(PluginInterface):
    def __init__(self, plugin_manager):
        super(Dummy,self).__init__(plugin_manager)
