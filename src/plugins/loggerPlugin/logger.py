import logging


try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import _
except :
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr


class LoggerWindow(QtGui.QWidget):
    def __init__(self):
        super(LoggerWindow,self).__init__()
        