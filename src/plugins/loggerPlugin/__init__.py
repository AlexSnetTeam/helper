#-*- coding:utf-8 -*-
from plugins import PluginInterface
import logging

class Logger(PluginInterface):
    def __init__(self, plugin_manager):
        super(Logger,self).__init__(plugin_manager)
        

    def initLoggerPlugin(self):
        self.loggerWindow = LoggerWindow()

        self.loggerAct = QtGui.QAction(_("Show logging console"),self.pm.getMainWindow(),triggered=self.showLoggingWindow)
        
        self.pm.getMainWindow().helpMenu.addAction(self.loggerAct)
        self.pm.getMainWindow().wm.addWidget(self.loggerWindow)

    def showLoggingWindow(self):
        self.pm.getMainWindow().wm.setCurrentWidget(self.loggerWindow)