#-*- coding:utf-8 -*-
import logging
import os, sys

from plugins import PluginInterface

logger = logging.getLogger('Helper.Plugins.stylePlugin')

class Style(PluginInterface):
    def __init__(self, plugin_manager):
        super(Style,self).__init__(plugin_manager)
        self.setupStylesheet()
        
    def setupStylesheet(self):
        ss = open(os.path.join(self.pm.getApplication().project_directory, 'resources/style_sheet.qss'))
        self.pm.getApplication().qtapp.setStyleSheet(ss.read())