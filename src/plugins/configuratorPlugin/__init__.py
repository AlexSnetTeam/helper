#-*- coding:utf-8 -*-
from plugins import PluginInterface

try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import _
except :
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr

class ConfiguratorWindow(QtGui.QWidget):

    pm = None # PluginManager
    mw = None # MainWindow
    modules = dict() # Modules Panels

    # Types
    PLUGIN = 1
    MODULE = 2

    # Signals
    onResize = QtCore.Signal(QtCore.QSize)

    def __init__(self):
        super(ConfiguratorWindow,self).__init__()

        from plugins import PluginManager
        self.pm = PluginManager()
        self.mw = self.pm.getMainWindow()

        self.box = QtGui.QHBoxLayout(self)
        self.box.setContentsMargins(0,0,0,0)
        self.setLayout(self.box)

        self.setupWidgets()

    def addSettingsPane(self,title,widget,paneType=None):
        if paneType == None: paneType = self.PLUGIN
        if paneType == self.PLUGIN:
            self.stack.addWidget(widget)
        elif paneType == self.MODULE:
            act = self.bar.createAction(title)
            act.triggered.connect(self.stack.setCurrentWidget(widget))

    def openModulePane(self):
        pass

    def openPluginPane(self,current,previsious):
        if previsious is not None:
            self.box.removeWidget( self.panes[previsious.text()] )
        self.box.addWidget( self.panes[current.text()] )
        print self.box.children()

    def resizeEvent(self, event):
        self.onResize.emit(event.size())

    def setupWidgets(self):
        self.optionBox = QtGui.QListWidget()
        self.box.addWidget(self.optionBox)

        self.stack = QtGui.QStackedWidget()
        self.box.addWidget(self.stack)


class Configurator(PluginInterface):
    configurationWindow = None

    def __init__(self, plugin_manager):
        super(Configurator, self).__init__(plugin_manager)
        
        self.configurationWindow = ConfiguratorWindow()

        self.preferencesAct = QtGui.QAction(_("Preferences..."),self.pm.getMainWindow(),triggered=self.showSettingsWindow)
        self.preferencesAct.setMenuRole(QtGui.QAction.PreferencesRole)
        self.preferencesAct.setShortcut(QtGui.QKeySequence.Preferences)

        self.pm.getMainWindow().editMenu.addAction(self.preferencesAct)
        self.pm.getMainWindow().wm.addWidget(self.configurationWindow)
        


    def showSettingsWindow(self):
        self.pm.getMainWindow().wm.setCurrentWidget(self.configurationWindow)
        # self.configurationWindow.show()
        # self.configurationWindow.setFocus()

    def addPane(self, *args, **kwargs):
        self.configurationWindow.addSettingsPane(*args,**kwargs)