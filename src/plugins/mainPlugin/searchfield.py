#-*- coding:utf-8 -*-

import sys, os

try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import _
except :
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr

from progressindicator import QProgressIndicator

class SuggestCompletion(QtCore.QObject):
    popup = None

    def __init__(self,parent=None):
        super(SuggestCompletion,self).__init__(parent)
        self.parent = parent
        self.parent.completer = self
        self.initUI()

    def __del__(self):
        del self.popup

    def initUI(self):
        self.popup = QtGui.QTreeWidget()
        self.popup.setWindowFlags(QtCore.Qt.Popup)
        self.popup.setFocusPolicy(QtCore.Qt.NoFocus)
        self.popup.setFocusProxy(self.parent)
        self.popup.setMouseTracking(True)

        self.popup.setColumnCount(2)
        self.popup.setUniformRowHeights(True)
        self.popup.setRootIsDecorated(False)
        self.popup.setEditTriggers(self.popup.NoEditTriggers)
        self.popup.setSelectionBehavior(self.popup.SelectRows)
        self.popup.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Plain)
        self.popup.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.popup.header().hide()

        self.popup.installEventFilter(self)

        self.popup.itemClicked.connect(self.doneCompletion)

        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.setInterval(500)

        self.timer.timeout.connect(self.autoSuggest)
        self.timer.timeout.connect(self.parent.searchStarted)

        self.parent.textEdited.connect(self.timer.start)

    def autoSuggest(self):
        str = self.parent.text()
        self.testtimer = QtCore.QTimer(self)
        self.testtimer.setSingleShot(True)
        self.testtimer.setInterval(800)
        self.testtimer.timeout.connect(self.showCompletion)

    def doneCompletion(self):
        self.timer.stop()
        self.popup.hide()
        self.parent.setFocus()
        item = self.popup.currentItem()
        if item:
            self.parent.setText(item.text(0))
            self.parent.emit(self.parent.returnPressed)

    def showCompletion(self):
        self.popup.move(self.parent.mapToGlobal(QtCore.QPoint(0, self.parent.height())))
        self.popup.setFocus()
        self.popup.show()

    def preventSuggest(self):
        self.timer.stop()

class SearchField(QtGui.QLineEdit):
    completer = None

    progressindicator = None

    def __init__(self,parent=None):
        super(SearchField,self).__init__(parent)
        #self.completer = SuggestCompletion(self)
        self.returnPressed.connect(self.doSearch)
        self.progressindicator = QProgressIndicator(self)
        self.progressindicator.show()

        #self.setContentsMargins(20,1,1,1)

    def resizeEvent(self, event):
        self.progressindicator.move(self.width()-self.progressindicator.width(),0)
        super(SearchField,self).resizeEvent(event)

    def searchStarted(self):
        self.progressindicator.startAnimation()

    def doSearch(self):
        if self.completer:
            self.completer.preventSuggest()
        self.progressindicator.stopAnimation()
        print 'enter'


if __name__ == '__main__':
    app = QtGui.QApplication([])
    le = SearchField()
    le.show()
    co = SuggestCompletion(le)
    #co.popup.move(100,10)
    #co.popup.show()
    sys.exit(app.exec_())