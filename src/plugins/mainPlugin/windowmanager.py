#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
 Window (tabs) manager Qt widget
 Author: Alexey Grechishkin (a.grechishkin@corp.mail.ru, me@alexsnet.ru)
"""

import sys, os
try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import _
except :
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr


class WindowManager(QtGui.QScrollArea):

    old_widget = None
    parent = None

    def __init__(self, parent=None):
        super(WindowManager,self).__init__(parent)
        self.parent = parent
        if self.parent:
            self.parent.onResize.connect(self.resize)

        #self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        #self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        #self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setStyleSheet("background:transparent;")

    def sizeHint(self):
        return self.parent.widgetAreaSize

    def changeTo(self, widget):
        # @TODO: добавить анимацию и тд
        self.setWidget(widget)

if __name__ == '__main__':
    qapp = QtGui.QApplication(sys.argv)

    w = WindowManager()
    w.show()

    sys.exit(qapp.exec_())