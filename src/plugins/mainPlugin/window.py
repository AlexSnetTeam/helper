#-*- coding:utf-8 -*-

import sys, os
import logging
try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import _
except:
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr

from plugins import PluginManager

logger = logging.getLogger('Helper.Plugins.mainPlugin')

class MainWindow(QtGui.QWidget):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.setMinimumSize(500, 300)
        self.resize(QtCore.QSize(500,300))

        self.setFocus()

        #self.setWindowIcon(QtGui.QIcon('./resources/atSign.png'))
        self.setWindowTitle(_('Helper'))

        self.setupLayout()
        self.setupSideBar()
        self.setupWinMngr()

        self.setupActions()
        self.setupMenuBar()

    def closeEvent(self, event):
        super(MainWindow, self).closeEvent(event)
        #self.hide()
        #event.ignore()

    def setupLayout(self):
        self.box = QtGui.QHBoxLayout(self)
        self.box.setContentsMargins(0, 0, 0, 0)
        #self.setLayout(self.box)

    def setupActions(self):
        self.minimizeAction = QtGui.QAction(_('Minimize'), self, triggered=self.hide)
        self.maximizeAction = QtGui.QAction(_('Ma&ximize'), self, triggered=self.showMaximized)
        self.restoreAction = QtGui.QAction(_('Restore'), self, triggered=self.showNormal)

        self.quitAction = QtGui.QAction(_('Quit'), self, triggered=QtGui.qApp.quit)
        self.quitAction.setShortcut(QtGui.QKeySequence.Quit)
        self.quitAction.setStatusTip(_("Close Helper"))

    def setupSideBar(self):
        from MRD.sidebar import sidebar
        self.sidebar = sidebar.Sidebar()
        #self.sidebar.texture = QtGui.QImage('resources/sidebarTexture.png')
        self.layout().addWidget(self.sidebar)

    def setupWinMngr(self):
        #from .windowmanager import WindowManager
        #self.wm = WindowManager(self)
        self.wm = QtGui.QStackedWidget()
        self.layout().addWidget(self.wm)
        self.wm.addWidget(QtGui.QPushButton("hello"))

    def setupMenuBar(self):
        self.menubar = QtGui.QMenuBar()
        self.layout().setMenuBar(self.menubar)

        self.helpMenu = QtGui.QMenu(_('&Help'))
        self.fileMenu = QtGui.QMenu(_('&File'))
        self.editMenu = QtGui.QMenu(_('&Edit'))
        self.windowMenu = QtGui.QMenu(_('&Window'))

        if not hasattr(self.pm.getApplication(), 'trayIconMenu'):
            self.pm.getApplication().trayIconMenu = QtGui.QMenu()

        self.pm.getApplication().trayIconMenu.addMenu(self.editMenu)
        self.menubar.addMenu(self.fileMenu)
        self.menubar.addMenu(self.editMenu)
        self.menubar.addMenu(self.windowMenu)
        self.menubar.addMenu(self.helpMenu)

        self.fileMenu.addAction(self.quitAction)
        # self.helpMenu.addAction(_('Application'))

