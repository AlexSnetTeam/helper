#-*- coding:utf-8 -*-
import logging
from plugins import PluginInterface

logger = logging.getLogger('Helper.plugins.mainPlugin')

class Main(PluginInterface):
    def __init__(self, plugin_manager):
        super(Main, self).__init__(plugin_manager)
        self.pm.getApplication().mainWindow = self.getMainWindow()
        self.pm.getApplication().mainWindow.show()
        self.pm.getApplication().mainWindow.update()
        self.pm.getApplication().mainWindow.setFocus()
        logger.warning(self.resourcePath)

    def unload(self):
        self.pm.getApplication().mainWindow.close()
        self.pm.getApplication().mainWindow.deleteLater()
        del self.pm.getApplication().mainWindow
        self.pm.getApplication().mainWindow = None

    def getMainWindow(self):
        if not hasattr(self, 'mw'):
            from .window import MainWindow
            MainWindow.pm = self.pm
            self.mw = MainWindow()
        return self.mw

    def addSidebarButton(self, action):
        self.mw.sidebar.addAction(action)
