#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
 Sidebar Qt widget
 Author: Alexey Grechishkin (a.grechishkin@corp.mail.ru, me@alexsnet.ru)
"""

import sys, os
try:
    from QTF import QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore


class Sidebar(QtGui.QWidget):

    _actions = list()
    _pressedAction = None
    _checkedAction = None

    checked = QtCore.Signal(QtGui.QAction)

    texture = None

    action_height = 90

    def __init__(self, parent=None):
        super(Sidebar, self).__init__(parent)
        self.texture = QtGui.QImage(os.path.join(os.path.dirname(__file__),'resources/sidebarTexture.png'))
        self.setFixedWidth(self.action_height)

    def setTexture(self, texture ):
        if type(texture) == type(str):
            self.texture = QtGui.QImage(texture)
        else:
            self.texture = texture

    def paintEvent(self, event):
        p = QtGui.QPainter(self)

        fontText = p.font()
        fontText.setFamily("Helvetica Neue")
        p.setFont(fontText)

        #print self.texture
        p.fillRect(event.rect(), QtGui.QBrush(self.texture))
        p.setPen(QtCore.Qt.black)
        p.drawLine(event.rect().topRight(), event.rect().bottomRight())

        actions_height = len(self._actions)*self.action_height
        action_y = event.rect().height()/2-actions_height/2

        for action in self._actions:
            actionRect = QtCore.QRect(0, action_y, event.rect().width(), self.action_height)
            if action.isChecked():
                p.setOpacity(0.5)
                p.fillRect(actionRect, QtGui.QColor(19, 19, 19))
                p.setPen(QtGui.QColor(9, 9, 9))
                p.drawLine(actionRect.topLeft(), actionRect.topRight())
                p.setOpacity(1)

            if action == self._actions[-1]:
                p.setPen(QtGui.QColor(15, 15, 15))
                p.drawLine(QtCore.QPoint(0, actionRect.bottomLeft().y()-1), QtCore.QPoint(actionRect.width(), actionRect.bottomRight().y()-1))
                p.setPen(QtGui.QColor(55, 55, 55))
                p.drawLine(actionRect.bottomLeft(), actionRect.bottomRight())

            if not action.isChecked():
                p.setOpacity(0.1)
                p.fillRect(actionRect, QtGui.QColor(128,128,128))
                p.setOpacity(1)
                p.setPen(QtGui.QColor(15, 15, 15))
                p.drawLine(actionRect.topLeft(), actionRect.topRight())
                p.setPen(QtGui.QColor(55, 55, 55))
                p.drawLine(QtCore.QPoint(0, actionRect.topLeft().y()+1), QtCore.QPoint(actionRect.width(), actionRect.topRight().y()+1))

            icon_size = 48
            actionIconRect = QtCore.QRect(0, action_y, event.rect().width(), self.action_height-20)
            actionIcon = QtGui.QIcon(action.icon())
            actionIcon.paint(p, actionIconRect)

            p.setPen(QtGui.QColor(217, 217, 217))
            if action.isChecked():
                p.setPen(QtGui.QColor(255, 255, 255))
            actionTextRect = QtCore.QRect(0, action_y+actionRect.height()-20, event.rect().width(), 15)
            p.drawText(actionTextRect, QtCore.Qt.AlignCenter, action.text())

            action_y += actionRect.height()

    def addAction(self, action):
        self._actions.append(action)
        action.setCheckable(True)
        self.update()

    def createAction(self, title, icon=None):
        action = None
        if icon is not None:
            action = QtGui.QAction(QtGui.QIcon(icon), unicode(title),self)
        else:
            action = QtGui.QAction(unicode(title),self)
        self.addAction(action)
        return action

    def setCurrentAction(self, action, force=False):

        if force:
            self._pressedAction = action

        if self._pressedAction is not action or action is None:
            self._pressedAction = None
            return
        if self._checkedAction is not None:
            self._checkedAction.setChecked(False)
        self._checkedAction = self._pressedAction
        if self._checkedAction is not None:
            self._checkedAction.setChecked(True)
        self.update()
        self._pressedAction = None

        action.toggle()
        action.trigger()
        self.checked.emit(action)

    def mousePressEvent(self, event):
        self._pressedAction = self.actionAt(event.pos())
        if self._pressedAction == None or self._pressedAction == self._checkedAction:
            return
        self.update()

    def mouseReleaseEvent(self, event):
        self.setCurrentAction(self.actionAt(event.pos()))

    def minimumSizeHint(self):
        return QtCore.QSize(90, len(self._actions)*self.action_height)

    def actionAt(self, at):
        actions_height = len(self._actions)*self.action_height
        action_y = self.rect().height()/2-actions_height/2

        for action in self._actions:
            actionRect = QtCore.QRect(0, action_y, self.rect().width(), self.action_height)
            if actionRect.contains(at):
                return action
            action_y += actionRect.height()
        return None

    def _testSlot(self):
        print 'Slot called'