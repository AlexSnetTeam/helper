#-*- coding:utf-8 -*-

import logging
import imp
import os
try:
    import queue
except ImportError:
    #python 2.x
    import Queue as queue
import reloader
import sys
import threading
import time

_win32 = (sys.platform == 'win32')

__all__ = ['PluginManager', 'PluginInterface']

logger = logging.getLogger('Helper.PluginManager')


def _normalize_filename(filename):
    if filename is not None:
        if filename.endswith('.pyc') or filename.endswith('.pyo'):
            filename = filename[:-1]
        elif filename.endswith('$py.class'):
            filename = filename[:-9] + '.py'
    return filename

class ModuleMonitor(threading.Thread):
    """Monitor module source file changes"""

    def __init__(self, manager, interval=1):
        threading.Thread.__init__(self)
        self.manager = manager
        self.daemon = True
        self.mtimes = {}
        self.queue = queue.Queue()
        self.interval = interval

    def run(self):
        while True:
            self._scan()
            time.sleep(self.interval)

    def _scan(self):
        # We're only interested in file-based modules (not C extensions).
        pluginDirs = [ os.path.realpath(os.path.dirname(m.__file__)) for m in sys.modules.values()
                if m and '__file__' in m.__dict__ and os.path.realpath(os.path.dirname(__file__)) in os.path.realpath(m.__file__)]
        modules = []
        for mod in pluginDirs:
            for dirname, dirnames, filenames in os.walk(mod):
                for filename in filenames:
                    if '.py' in filename:
                        modules.append(os.path.join(mod, filename))
        for filename in modules:
            # We're only interested in the source .py files.
            filename = _normalize_filename(filename)

            # stat() the file.  This might fail if the module is part of a
            # bundle (.egg).  We simply skip those modules because they're
            # not really reloadable anyway.
            try:
                stat = os.stat(filename)
            except OSError:
                continue

            # Check the modification time.  We need to adjust on Windows.
            mtime = stat.st_mtime
            if _win32:
                mtime -= stat.st_ctime

            # Check if we've seen this file before.  We don't need to do
            # anything for new files.
            if filename in self.mtimes:
                # If this file's mtime has changed, queue it for reload.
                if mtime != self.mtimes[filename]:
                    pluginname = filename.split(os.path.separator)[-2]
                    logger.info("Plugin %s has been changed." % pluginname)
                    self.queue.put(filename)

            # Record this filename's current mtime.
            self.mtimes[filename] = mtime

# Interfaces
class PluginInterface(object):

    # Each of plugin must be in one instance only

    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(PluginInterface, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, plugin_manager=None):
        self.depends_on = list()
        self.title = None
        self.settings = dict()
        self.resourcePath = None

        self.pm = plugin_manager
        self.settings = self.pm.readPluginSettings(self.__class__.__name__.lower())
        for dep in self.settings['dependencies']['plugins']:
            self.depends_on.append(dep['name'])
        self.title = self.settings['title']
        self.resourcePath = os.path.join(os.path.join(os.path.realpath(os.path.dirname(__file__)), "%s%s"% \
                                            (os.path.splitext(self.title.lower())[0], 'Plugin') ),"resources")
        if hasattr(self, 'load'):
            if callable(self.load): self.load()

    def unload(self):
        pass

# Plugin manager
class PluginManager(object):

    _plugins = dict()
    _plugin_settings = dict()
    _application = None

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(PluginManager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, application=None):
        if application is not None:
            self._application = application

        self.monitor = ModuleMonitor(self, interval=1)
        self.monitor.start()

    def __del__(self):
        plugins = self.loadedPlugins()
        for plugin in plugins:
            self.unloadPlugin(plugin)

    def versioncmp(self, a, b):
        from pkg_resources import parse_version as V
        return cmp(V(a), V(b))

    def getApplication(self):
        return self._application

    def getMainWindow(self):
        return self._application.mainWindow

    def getPlugin(self, plugin):
        if plugin in self._plugins.keys():
            return self._plugins[plugin]
        return False

    def __getattr__(self, item):
        if str(item).endswith('Plugin'):
            return self.getPlugin(str(item).replace('Plugin', ''))
        else:
            return super(PluginManager, self).__getattribute__(item)



    def loadPlugin(self, pluginName):
        if pluginName in self._plugins.keys(): return True
        importtmp = False
        try:
            try:
                importtmp = __import__('%s%s'%(os.path.splitext(pluginName)[0], 'Plugin'), None, None, [''])
            except ImportError, e:
                logger.warning('Plugin %s can not be loaded: %s' % (pluginName, e))
                return False
            except AttributeError, e:
                logger.warning('Plugin %s cause an attribute error: %s' % (pluginName, e))
                return False
            except Exception, e:
                logger.error('Plugin %s has errors: %s' % (pluginName, e))
                return False
            else:
                pass
            finally:
                if importtmp is False:
                    return False
                settings = self.readPluginSettings(pluginName)
                if not settings:
                    return False
                if settings['noload']:
                    logger.info('Plugin %s is restricted'%pluginName)
                    return False

                plcl = getattr(importtmp, settings['title'])
                self._plugins[pluginName] = plcl(self)
                #   if hasattr(importtmp, 'depends_on'):
                #        logger.info('Loading %s\'s dependencies' % self._plugins[pluginName].title)
                #        if hasattr(self._plugins[pluginName],'depends_on'):
                #            for dep in self._plugins[pluginName].depends_on:
                #                if self.loadPlugin(dep):
                #                    continue
                #                else:
                #                    self.unloadPlugin()
                #                    return False
                logger.info('Loading plugin %s done' % self._plugins[pluginName].title)
                return True
        except Exception, e:
            logger.error('Fail to load plugin "%s", because of %s'%(pluginName,e))

    def readPluginSettings(self, pluginName):
        if self._plugin_settings.has_key(pluginName):
            return self._plugin_settings[pluginName]
        from bs4 import BeautifulSoup
        filePath = 'plugins%s%s%s%s%s'%(os.path.sep,os.path.splitext(pluginName)[0],'Plugin',os.path.sep,'Info.xml')
        if not os.path.exists(filePath):
            del sys.modules['%sPlugin'%pluginName]
            logger.warning('Can not find plugin settings file "%s"'%filePath)
            return False
        info = dict()
        soup = BeautifulSoup(file(filePath).read(), from_encoding='utf-8')
        info['title'] = soup.find('plugin')['title']
        info['version'] = soup.find('plugin')['version']
        info['noload'] = True if soup.find('noload') else False
        info['description'] = soup.find('description').contents[0] if soup.find('description') else ''
        dependencies = { 'plugins':list(), 'packages':list() }
        if soup.find('dependencies'):
            if soup.find('dependencies').find_all('plugin'):
                for dep in soup.find('dependencies').find_all('plugin'):
                    plugindep = {
                        'name' : dep['name'],
                        'version' : None # @TODO: version
                    }
                    dependencies['plugins'].append(plugindep)
            if soup.find('dependencies').find_all('package'):
                for dep in soup.find('dependencies').find('package'):
                    # @TODO: сделать проверку зависимостей от пакетов
                    pass
        info['dependencies'] = dependencies

        del soup
        self._plugin_settings[pluginName] = info
        print info
        return info



    def loadPlugins(self):
        pluginCandidates = os.listdir(os.path.realpath(os.path.dirname(__file__)))

        for s in pluginCandidates:
            if 'Plugin' not in s: continue
            logger.info('Found plugin directory %s' % s)
            # Просто передаем найденный пакет методу загрузки плагина, он разберется
            self.loadPlugin(s.replace('Plugin', ''))



    def unloadPlugin(self, pluginName):
        if pluginName not in self._plugins:
            logger.warning('Trying to unload not loaded plugin %s' % pluginName)
            return False
        logger.info('Unloading  plugin %s' % pluginName)
        plugins = self._plugins.keys()

        for p in plugins:
            if pluginName == p: continue
            if pluginName in self._plugins[p].depends_on:
                self.unloadPlugin(p)

        if "onUnload" in dir(self._plugins[pluginName]):
            self._plugins[pluginName].onUnload()
            logger.info('Plugin %s onUnload called' % plugins[pluginName].name)

        self._plugins[pluginName].unload()
        del self._plugins[pluginName]

        instances = list()
        for pc in sys.modules:
            if '%sPlugin'%pluginName in pc:
                instances.append(pc)
        for pc in instances:
            del sys.modules[pc]
        del instances

        logger.info('Plugin %s unloaded' % pluginName)

    def reloadPlugin(self, pluginName):
        logger.info('Reloading plugin %s' % pluginName)
        loadedPlugins = self._plugins.keys()
        self.unloadPlugin(pluginName)
        for p in loadedPlugins:
            self.loadPlugin(p)


    def loadedPlugins(self):
        return self._plugins.keys()
