#-*- coding:utf-8 -*-
from plugins import PluginInterface

try:
    from QTF import QtCore
    from QTF import QtGui
    from QTF import Qt as QT
    from QTF import _
except :
    from PySide import QtCore, QtGui
    _ = QtCore.QObject().tr

class UpdaterWindow(QtGui.QWidget):

    pm = None # PluginManager
    mw = None # MainWindow

    def __init__(self):
        super(UpdaterWindow, self).__init__()

        from plugins import PluginManager
        self.pm = PluginManager()
        self.mw = self.pm.getMainWindow()

        self.setupWidgets()
        self.updatePluginList()

    def setupWidgets(self):
        self.vbox = QtGui.QVBoxLayout(self)
        self.setLayout(self.vbox)

        self.tabs = QtGui.QTabWidget(self)
        self.vbox.addWidget(self.tabs)

        # ##########
        # Manage tab
        # ##########
        self.installedLabel = QtGui.QLabel(_('There are all currently installed plugins.' +
                                             '\nHere you can check plugins that you will use or not.\n' +
                                             'Also you can see updates if available.'))

        self.manageTab = QtGui.QWidget(self.tabs)
        self.manageTabLayout = QtGui.QVBoxLayout(self.manageTab)
        self.manageTab.setLayout(self.manageTabLayout)

        self.pluginList = QtGui.QTableWidget(self.manageTab)
        self.pluginList.setColumnCount(2)
        self.pluginList.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.pluginList.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)
        self.pluginList.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Interactive)
        self.pluginList.horizontalHeader().hide()
        self.pluginList.verticalHeader().hide()
        self.pluginList.setShowGrid(False)
        self.pluginList.itemSelectionChanged.connect(self.manageTabSelection)

        self.manageTabDescription = QtGui.QTextEdit(self.manageTab)

        self.manageTabHLayout = QtGui.QHBoxLayout(self.manageTab)
        self.manageTabBtnLayout = QtGui.QHBoxLayout(self.manageTab)
        #self.setEnabled(False)

        self.manageTabLayout.addWidget(self.installedLabel)
        self.manageTabLayout.addLayout(self.manageTabHLayout)
        self.manageTabLayout.addLayout(self.manageTabBtnLayout)
        self.manageTabHLayout.addWidget(self.pluginList)
        self.manageTabHLayout.addWidget(self.manageTabDescription)

        # ###############
        # New plugins tab
        # ###############
        self.newPluginsTab = QtGui.QWidget(self.tabs)

        # ###########
        # Adding tabs
        # ###########
        self.tabs.addTab(self.manageTab, _('Manage plugins'))
        self.tabs.addTab(self.newPluginsTab, _('Plugins dir'))

    def updatePluginList(self):
        self.oddColor = QtGui.QColor(220, 220, 220)

        plugins = self.pm._plugin_settings.keys()
        loaded = self.pm._plugins.keys()
        plugins.sort()
        self.pluginList.setRowCount(len(plugins))
        self.pluginList.setEditTriggers( QtGui.QTableWidget.NoEditTriggers )
        i = 0
        for plugin in plugins:
            settings = self.pm._plugin_settings[plugin]

            title = QtGui.QTableWidgetItem(settings['title'])
            title.setCheckState(QtCore.Qt.Unchecked)

            if plugin in loaded:
                title.setCheckState(QtCore.Qt.Checked)
            if settings['noload']:
                title.setForeground(QtGui.QColor(200,128,128))

            version = QtGui.QTableWidgetItem(settings['version'])
            actions = QtGui.QTableWidgetItem()


            self.pluginList.setItem(i, 0, title)
            self.pluginList.setItem(i, 1, version)
            self.pluginList.setItem(i, 2, actions)

            if (i + 1) % 2 == 0:
                for j in range(0,self.pluginList.columnCount()):
                    if self.pluginList.item(i, j):
                        self.pluginList.item(i, j).setBackground(self.oddColor)
            i += 1

        self.pluginList.resizeColumnsToContents()
        self.pluginList.update()

    def manageTabSelection(self):
        item = self.pluginList.item(self.pluginList.currentRow(),0)
        print item.checkState()
        plugin = item.text().lower()
        settings = self.pm._plugin_settings[plugin]

        self.manageTabDescription.setText( '<h1>%s</h1><span>Version: %s</span><p>Description:<br />%s</p><p>Depends on:<ul><li></li></ul></p>' % (
            settings['title'],
            settings['version'],
            settings['description']
        ))

        if settings['dependencies']['plugins']:
            depends = '<table><thead><tr><td>Plugin</td><td>Version</td></tr></thead><tbody>'
            for depend in  settings['dependencies']['plugins']:
                depends += '<tr><td>%s</td><td>%s</td></tr>' % (depend['name'], depend['version'])
            depends += '</tbody></table>'
            self.manageTabDescription.append(depends)



class Updater(PluginInterface):
    def __init__(self, plugin_manager):
        super(Updater,self).__init__(plugin_manager)
        self.initConfPlugin()

    def initConfPlugin(self):
        self.winInst = UpdaterWindow()
        self.pm.getPlugin('configurator').addPane(_('Plugin Manager'),self.winInst)