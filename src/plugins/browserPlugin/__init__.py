#-*- coding:utf-8 -*-
from plugins import PluginInterface

from QTF import QtCore
from QTF import QtGui
from QTF import _

# Main plugin class must be equal to plugin title, which declared in Info.xml
class Browser(PluginInterface):
    def __init__(self, plugin_manager):
        super(Browser,self).__init__(plugin_manager)

        from .window import BrowserWindow

        self.browser = BrowserWindow()
        # self.browser.show()

        self.browserAct = QtGui.QAction(QtGui.QIcon("%s/icon.png"%self.resourcePath), _("Browser"),self.pm.getMainWindow(),triggered=self.showBrowser)
        self.pm.getMainWindow().wm.addWidget(self.browser)
        self.pm.mainPlugin.addSidebarButton(self.dashboardAct)

    def showBrowser(self):
        self.pm.getMainWindow().wm.setCurrentWidget(self.browser)